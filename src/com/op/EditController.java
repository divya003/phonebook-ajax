package com.op;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.*;
import com.op.Edit;
import com.op.DuplicateCheck;

/**
 * Servlet implementation class EditController
 */
@WebServlet("/editcontroller")
public class EditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String phone=request.getParameter("phone");
		String status=null;
		try
		{
			if(name==""||name.trim()=="")
				status="Don't make me nameless";
			else if(phone==""||phone.trim()=="")
				status="Allow people to call me on a vilid 10 digit no.";
			else if(phone.length()!=10)
				status="10 numbers required";
			else if(new DuplicateCheck().duplicate_check(phone)==3)
			{
				status="Different people with different numbers";
								
			}
			else
			{
				Edit obj=new Edit();
				status=obj.edit_values(id,name,phone);
				
			}
			Map<String,Object> map =new HashMap<String,Object>();
			map.put("status",status);
			write(response,map);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		}

		private void write(HttpServletResponse response,Map<String,Object> map) throws IOException
		{
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(new Gson().toJson(map));
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
}
