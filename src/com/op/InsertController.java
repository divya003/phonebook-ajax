package com.op;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import com.op.Insert;
import com.op.DuplicateCheck;
import com.google.gson.*;


/**
 * Servlet implementation class InsertController
 */
@WebServlet("/insert")
public class InsertController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String rtmsg=null;
		String status=null;
		response.setContentType("application/json");
		String name=request.getParameter("name");
		String phone=request.getParameter("phone");
		try
		{
		Map<String,Object> map=new HashMap<String,Object>();
		boolean nameisvalid=true;
		boolean phoneisinvalid=false;
		
		for(int i=0;i<name.length();i++)
		{
			if(Character.isLetter(name.charAt(i))==false)
			{
				nameisvalid=false;
				break;
			}
				
		}

		for(int i=0;i<phone.length();i++)
		{
			if(Character.isLetter(phone.charAt(i))==true)
			{
				phoneisinvalid=true;
				break;
			}
				
		}

		
		if(name==null||name.trim().length()==0)
			{
			rtmsg="Enter a name that can be spelled";
			status="failure";
			}
		else if(phone==null||phone.trim().length()==0)
			{
			rtmsg="Enter a phone no. that can be dialed";
			status="failure";
			}
		else if(nameisvalid==false)
			{
			rtmsg="Names don't look nice with numbers.";
			status="failure";
			}
		else if(phoneisinvalid==true)
			{
			rtmsg="Phone nos. with alphabets don't work.";
			status="failure";
			}
		else if(phone.length()!=10)
			{
			rtmsg="10 numbers required for a working phone no.";
			status="failure";
			
			}
		else if(new DuplicateCheck().duplicate_check(phone)>0)
		{
			rtmsg="Different people with different numbers";
			status="failure";
			
		}
		else
		{
			Insert obj=new Insert();
			int c=obj.insert_values(name,phone);
			if(c>0)
			{
				rtmsg="Insertion successful";
				status="success";	
			}
					

		}
		map.put("status", status);
		map.put("rtmsg", rtmsg);
		write(response,map);
		
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	private void write(HttpServletResponse response,Map<String,Object> map) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	}
}