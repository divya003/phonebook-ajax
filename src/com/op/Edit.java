package com.op;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.dpn.DB_Connection;
import java.util.*;


public class Edit {

	public Edit() {
		// TODO Auto-generated constructor stub
	}
	public Map<String,Object> get_edit_values(String id)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		Map<String,Object> map=new HashMap<String,Object>();
		
		//Contact_Bean beanobj=new Contact_Bean();
		try
		{
			String query="SELECT * FROM details WHERE id=?";
			
			ps=connection.prepareStatement(query);
			ps.setString(1,id);
			rs=ps.executeQuery();
			while(rs.next())
			{
				map.put("id", rs.getString("id"));
				map.put("name", rs.getString("name"));
				map.put("phone", rs.getString("phone"));
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return map;
	}
	

	public String edit_values(String id,String name,String phone)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		String status=null;
		try
		{
			String query="UPDATE details SET name=?,phone=? WHERE id=?";
			
			ps=connection.prepareStatement(query);
			ps.setString(3,id );
			ps.setString(1,name );
			ps.setString(2,phone );
			int rowcount=ps.executeUpdate();
			
			if(rowcount>0)
				status="Edited";
			else
				status="Edit failed";
		
				
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return status;
	}

}
