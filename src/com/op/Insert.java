package com.op;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.dpn.DB_Connection;
import com.dpn.Contact_Bean;
public class Insert {

	public  int insert_values(String name,String phone)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		int f=0;
		try
		{
			
			String query="INSERT INTO details(name,phone) VALUES(?,?)";
			ps=connection.prepareStatement(query);
			ps.setString(1,name);
			ps.setString(2,phone);
			int rowcount=ps.executeUpdate();
			if(rowcount>0)
				f=1;
			else
				f=1;
			connection.close();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return f;
	}


}
