package com.op;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.op.Read;
import java.util.*;
import com.google.gson.*;
import java.sql.ResultSet;

/**
 * Servlet implementation class ReadController
 */
@WebServlet("/read")
public class ReadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Read obj=new Read();
		ResultSet rs=obj.get_Values();
		ArrayList<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		try
		{
			while(rs.next())
			{
				String id=Integer.toString(rs.getInt("id"));
				String name=rs.getString("name");
				String phone=rs.getString("phone");
				Map<String,Object> m=new HashMap<String,Object>();
				m.put("id", id);
				m.put("name", name);
				m.put("phone", phone);
				list.add(m);
				
			}	
			write(response,list);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void write(HttpServletResponse response,ArrayList<Map<String,Object>> list) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(list));
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
