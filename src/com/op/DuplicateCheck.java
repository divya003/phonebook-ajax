package com.op;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.dpn.DB_Connection;
public class DuplicateCheck {

	public DuplicateCheck() {
		// TODO Auto-generated constructor stub
	}
	public int duplicate_check(String phone)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		int f=0;
		try
		{
			String query="SELECT * FROM details WHERE phone=?";
			
			ps=connection.prepareStatement(query);
			ps.setString(1,phone);
			ResultSet rs=ps.executeQuery();
			while(rs.next())
				f++;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return f;
	}


}
