package com.op;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.dpn.DB_Connection;
import com.dpn.Contact_Bean;
public class Delete {

	public Delete() {
		// TODO Auto-generated constructor stub
	}
	public int delete_values(String id)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		int f=0;
		try
		{
			String query="DELETE FROM details WHERE id=?";
			
			ps=connection.prepareStatement(query);
			ps.setString(1,id);
			int rowcount=ps.executeUpdate();
			if(rowcount>0)
				f=1;
			else
				f=1;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return f;
	}


}
