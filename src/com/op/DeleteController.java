package com.op;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class DeleteController
 */
@WebServlet("/delete")
public class DeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rtmsg=null;
		String status=null;
		response.setContentType("application/json");
		String id=request.getParameter("id");
		try
		{
		Map<String,Object> map=new HashMap<String,Object>();
		
		
			Delete obj=new Delete();
			int c=obj.delete_values(id);
			if(c>0)
			{
				rtmsg="Deletion successful";
				status="success";	
			}
					

		
		map.put("status", status);
		map.put("rtmsg", rtmsg);
		write(response,map);
		
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	private void write(HttpServletResponse response,Map<String,Object> map) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	}

	

}
